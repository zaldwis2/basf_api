<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\AnswerCheckins;
use App\Models\AnswerCheckouts;
use App\Models\CheckinSection;
use App\Models\CheckoutSection;
use App\Models\Driver;
use App\Models\Evidences;
use App\Models\GeneralInfoForm;
use App\Models\MasterCheckin;
use App\Models\MasterCheckout;
use App\Models\Privilieges;
use App\Models\Signs;
use App\Models\TransporterTypes;
use App\Models\Users;


class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    protected $image_sign = '/public/image-sign/';
    protected $image_evidence = '/public/image-evidence/';

    public function register(Request $request){
        try {
            $user = new Users();
            $user->id_employee = $request->id_employee;
            $user->id_privilege = $request->id_privilege;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->gender = $request->gender;
            $user->image = $request->image;
            $user->phone = $request->phone;
            $user->birth_date = $request->birth_date;
            $user->position = $request->position;
            $user->saveOrFail();

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil mendaftar',
                'dataUser' => [$user]
            ];    
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function login2(Request $request){
        $password = $request->password;
        try{
            $query=Users::where('id_employee','=',$request->id_employee)->first();
            if(!$query){
                $statusCode = 404;                    
                $response = [
                ];       
            } else {
                $dataPassword = $query->password;
                if(Hash::check($password, $dataPassword)){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Login Berhasil',
                        'dataUser' => [$query]
                    ];    
                } else {
                    $statusCode = 404;                    
                    $response = [
                        'error' => true,
                        'message' => 'NIK atau Password Salah',
                    ];       
                }
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Login Gagal';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getDataUser(Request $request){
        try{
            $data=Users::where('id','=',$request->id)->first();
            
            if(!$data){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'NIK atau Password Salah'
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil',
                    'dataUser' => [$data]
                ];
            }  
    
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateDataUser(Request $request){
            try{
                $dataUser=Users::where('id','=',$request->id)->first();        
    
                if($request->password != ""){
                    $dataUser->password = Hash::make($request->password);
                }
                if($request->name != ""){
                    $dataUser->name = $request->name;
                }
                if($request->email != ""){
                    $dataUser->email = $request->email;
                }
                if($request->phone != ""){
                    $dataUser->phone = $request->phone;
                }
    
                $dataUser->saveOrFail();
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'update berhasil',
                ];
    
            }catch (Exception $e){
    
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal update',
                ];    
            }finally{
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function getActivity(){
            try{
                $current_date = new \DateTime();
                $dt = $current_date->format('Y-m-d');
                $data = DB::table('general_info_form')
                    ->join('users', 'general_info_form.approver_id_checkin', '=', 'users.id')
                    ->join('drivers', 'general_info_form.driver_id', '=', 'drivers.id')
                    ->join('transporter_types', 'general_info_form.transporter_type', '=', 'transporter_types.id')
                    ->select('users.name as approver_name','transporter_types.type as transporter_name','drivers.name as driver_name',
                    'drivers.phone','drivers.license_type','drivers.license_number','drivers.license_expiry_date','general_info_form.*')
                    ->where('general_info_form.created_at', 'LIKE', '%' . $dt . '%')
                    ->where('general_info_form.isDeleted', '0')
                    ->orderBy('general_info_form.created_at', 'ASC')
                    ->get();

                
                if(!$data->isEmpty()){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'listActivity',
                        'activityList' => $data,
                    ];       
                }else{
                    $statusCode = 404;
                    $response = [
                    'error' => true,
                    'message' => 'Fail',
                ];     
                } 
            }catch (Exception $e){
    
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Fail',
                ];    
            }finally{
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function getCheckinCount(){
        try{
            $current_date = new \DateTime();
            $dt = $current_date->format('Y-m-d');
            $data = DB::table('general_info_form')
                ->select('*')
                ->where('general_info_form.created_at', 'LIKE', '%' . $dt . '%')
                ->where('isOpen','1')
                ->where('isDeleted','0')
                ->count();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Checkin Truck',
                    'countCheckin' => $data,
                ];     
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
                ];    
            }         
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getParentCheckin(){
        try{
            $data = DB::table('check_in_section')
                ->select('id','description')
                ->get();
            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'list parent checkin',
                    'parentCheckin' => $data,
                ];         
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Fail',
                ];   
            }     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getChildCheckin(Request $request){
        try{
            $data = DB::table('master_check_in')
                ->select('id','desc_check_in')
                ->where('section_id',$request->section_id)
                ->get();
            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'list parent checkin',
                    'childCheckin' => $data,
                ];      
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
                ];    
            }     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    public function getTransporterType(){
        try{
            $data = DB::table('transporter_types')
                ->select('id','type')
                ->get();

            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'list transporter type',
                    'transporterType' => $data,
                ];     
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Fail',
                ];    
            }      
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getDriver(Request $request){
        try{
            $data = DB::table('drivers')
                ->select('*')
                ->where('license_number',$request->license_number)
                ->get();
            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'driver data',
                    'driverData' => $data,
                ];       
            }else{
                $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
            }
                    
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function saveForm(Request $request){
        try{
            $form = new GeneralInfoForm();
            $driver = new Driver();
            $driverID;
            $formID;

            $dataDriver=Driver::where('license_number','=',$request->license_number)->first();
            if(!$dataDriver){
                $driver->name = $request->driver_name;
                $driver->license_type = $request->license_type;
                $driver->license_number = $request->license_number;
                $driver->phone = $request->phone;
                $driver->is_induction = $request->is_induction;
                $driver->license_expiry_date = $request->license_expiry_date;
                $driver->saveOrFail();
                $driverID = $driver->id;
    
                $form->vehicle_number = $request->vehicle_number;
                $form->tank_number = $request->tank_number;
                $form->DO_number = $request->DO_number;
                $form->product_name = $request->product_name;
                $form->vehicle_certificate_expiry_date = $request->vehicle_certificate_expiry_date;
                $form->kir_expiry_date =$request->kir_expiry_date;
                $form->vehicle_type = $request->vehicle_type;
                $form->carrier_name = $request->carrier_name;
                $form->transporter_type = $request->transporter_type;
                $form->approver_id_checkin = $request->approver_id;
                $form->driver_id = $driverID;
                $form->annotation = $request->annotation;
                $form->destination = $request->destination;
                $form->saveOrFail();
                $formID = $form->id;    
            }else{
                $driverID = $dataDriver->id;
                $dataDriver->is_induction = $request->is_induction;
                $dataDriver->saveOrFail();

                $form->vehicle_number = $request->vehicle_number;
                $form->tank_number = $request->tank_number;
                $form->DO_number = $request->DO_number;
                $form->product_name = $request->product_name;
                $form->vehicle_certificate_expiry_date = $request->vehicle_certificate_expiry_date;
                $form->kir_expiry_date =$request->kir_expiry_date;
                $form->vehicle_type = $request->vehicle_type;
                $form->carrier_name = $request->carrier_name;
                $form->transporter_type = $request->transporter_type;
                $form->approver_id_checkin = $request->approver_id;
                $form->driver_id = $driverID;
                $form->annotation = $request->annotation;
                $form->destination = $request->destination;
                $form->saveOrFail();
                $formID = $form->id;
            }
    
            if($form){            
                $sign1 = new Signs();
                if ($request->hasFile('image_approver')) {
                    if ($request->file('image_approver')->isValid()) {
                        $file_ext        = $request->file('image_approver')->getClientOriginalExtension();
                        $file_size       = $request->file('image_approver')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_sign;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image_approver')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $approver_sign_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image_approver')->move($dest_path, $approver_sign_image);
                            $sign1->image= $approver_sign_image;
                        }
                    }
                    $sign1->id_form = $formID;
                    $sign1->type = 'Approver';
                    $sign1->status = 'Checkin';
                    $sign1->saveOrFail();
                }
                $sign2 = new Signs();
                if ($request->hasFile('image_driver')) {
                    if ($request->file('image_driver')->isValid()) {
                        $file_ext        = $request->file('image_driver')->getClientOriginalExtension();
                        $file_size       = $request->file('image_driver')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_sign;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image_driver')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $driver_sign_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image_driver')->move($dest_path, $driver_sign_image);
                            $sign2->image= $driver_sign_image;
                        }
                    }
                    $sign2->id_form = $formID;
                    $sign2->type = 'Driver';
                    $sign2->status = 'Checkin';
                    $sign2->saveOrFail();
                }       
                
                $evidence1 = new Evidences();
                if ($request->hasFile('evidence1')) {
                    if ($request->file('evidence1')->isValid()) {
                        $file_ext        = $request->file('evidence1')->getClientOriginalExtension();
                        $file_size       = $request->file('evidence1')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_evidence;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('evidence1')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $evidence1_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('evidence1')->move($dest_path, $evidence1_image);
                            $evidence1->image= $evidence1_image;
                        }
                    }
                    $evidence1->id_form = $formID;
                    $evidence1->saveOrFail();
                }
                $evidence2 = new Evidences();
                if ($request->hasFile('evidence2')) {
                    if ($request->file('evidence2')->isValid()) {
                        $file_ext        = $request->file('evidence2')->getClientOriginalExtension();
                        $file_size       = $request->file('evidence2')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_evidence;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('evidence2')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $evidence2_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('evidence2')->move($dest_path, $evidence2_image);
                            $evidence2->image= $evidence2_image;
                        }
                    }
                    $evidence2->id_form = $formID;
                    $evidence2->saveOrFail();
                }
                $evidence3 = new Evidences();
                if ($request->hasFile('evidence3')) {
                    if ($request->file('evidence3')->isValid()) {
                        $file_ext        = $request->file('evidence3')->getClientOriginalExtension();
                        $file_size       = $request->file('evidence3')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_evidence;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('evidence3')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $evidence3_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('evidence3')->move($dest_path, $evidence3_image);
                            $evidence3->image= $evidence3_image;
                        }
                    }
                    $evidence3->id_form = $formID;
                    $evidence3->saveOrFail();
                } 
                $evidence4 = new Evidences();      
                if ($request->hasFile('evidence4')) {
                    if ($request->file('evidence4')->isValid()) {
                        $file_ext        = $request->file('evidence4')->getClientOriginalExtension();
                        $file_size       = $request->file('evidence4')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_evidence;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('evidence4')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $evidence4_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('evidence4')->move($dest_path, $evidence4_image);
                            $evidence4->image= $evidence4_image;
                        }
                    }
                    $evidence4->id_form = $formID;
                    $evidence4->saveOrFail();
                } 
                $evidence5 = new Evidences();
                if ($request->hasFile('evidence5')) {
                    if ($request->file('evidence5')->isValid()) {
                        $file_ext        = $request->file('evidence5')->getClientOriginalExtension();
                        $file_size       = $request->file('evidence5')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_evidence;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('evidence5')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $evidence5_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('evidence5')->move($dest_path, $evidence5_image);
                            $evidence5->image= $evidence5_image;
                        }
                    }
                    $evidence5->id_form = $formID;
                    $evidence5->saveOrFail();
                }
            }

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Activity Created',
                    'form_id' => $formID
                ];

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail Create Activity'
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function saveAnswer(Request $request){
        try {
           foreach ($request->input('answer_data') as $key => $value) {               
                    $answerCheckin = new AnswerCheckins();
                    $answerCheckin->approve_check_in = $value["approve_check_in"];
                    $answerCheckin->decline_check_in = $value["decline_check_in"];
                    $answerCheckin->na_check_in = $value["na_check_in"];
                    $answerCheckin->annotation_check_in = $value["annotation_check_in"];
                    $answerCheckin->general_info_id = $request->form_id;
                    $answerCheckin->master_check_in_id = $value["answer_id"];
                    $answerCheckin->saveOrFail();
            }     
           
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Success'
            ];    
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function addNCM(Request $request){
        try{
            $dataUser=GeneralInfoForm::where('id','=',$request->id)->first();        
            $dataUser->NCM = $request->ncm;
            $dataUser->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function saveAnswerCheckout(Request $request){
        try {
            foreach ($request->input('answer_data') as $key => $value) {               
                     $answerCheckout = new AnswerCheckouts();
                     $answerCheckout->approve_check_out = $value["approve_check_out"];
                     $answerCheckout->decline_check_out = $value["decline_check_out"];
                     $answerCheckout->na_check_out = $value["na_check_out"];
                     $answerCheckout->annotation_check_out = $value["annotation_check_out"];
                     $answerCheckout->general_info_id = $request->form_id;
                     $answerCheckout->master_check_out_id = $value["answer_id"];
                     $answerCheckout->saveOrFail();
             }     
            
             $statusCode = 200;
             $response = [
                 'error' => false,
                 'message' => 'Success'
             ];    
         } catch (Exception $e) {
             $statusCode = 500;
             $response = [
                 'error' => true,
                 'message' => 'Server error',
             ];    
         }finally{
             return response($response,$statusCode)->header('Content-Type','application/json');
         }
    }

    public function saveSignCheckout(Request $request){
        try {
            $current_date = new \DateTime();
            $dataForm=GeneralInfoForm::where('id','=',$request->id_form)->first();
            if($dataForm->checkout_datetime == null){
                $dataForm->approver_id_checkout = $request->approver_id_checkout;
                $dataForm->isOpen = '0';  
                $dataForm->checkout_datetime = $current_date;
                $dataForm->saveOrFail();
    
                $sign1 = new Signs();
                if ($request->hasFile('image_approver')) {
                    if ($request->file('image_approver')->isValid()) {
                        $file_ext        = $request->file('image_approver')->getClientOriginalExtension();
                        $file_size       = $request->file('image_approver')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_sign;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image_approver')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $approver_sign_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image_approver')->move($dest_path, $approver_sign_image);
                            $sign1->image= $approver_sign_image;
                        }
                    }
                    $sign1->id_form = $request->id_form;
                    $sign1->type = 'Approver';
                    $sign1->status = 'Checkout';
                    $sign1->saveOrFail();
                }
                $sign2 = new Signs();
                if ($request->hasFile('image_driver')) {
                    if ($request->file('image_driver')->isValid()) {
                        $file_ext        = $request->file('image_driver')->getClientOriginalExtension();
                        $file_size       = $request->file('image_driver')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_sign;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image_driver')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $driver_sign_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image_driver')->move($dest_path, $driver_sign_image);
                            $sign2->image= $driver_sign_image;
                        }
                    }
                    $sign2->id_form = $request->id_form;
                    $sign2->type = 'Driver';
                    $sign2->status = 'Checkout';
                    $sign2->saveOrFail();
                }
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Success'
                ];           
            }else{
                $statusCode = 500;
                $response = [
                    'error' => true,
                    'message' => 'Server error',
                ];    
            }
         } catch (Exception $e) {
             $statusCode = 500;
             $response = [
                 'error' => true,
                 'message' => 'Server error',
             ];    
         }finally{
             return response($response,$statusCode)->header('Content-Type','application/json');
         }
    }

    public function getParentCheckout(){
        try{
            $data = DB::table('check_out_section')
                ->select('id','description')
                ->get();
            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'list parent checkin',
                    'parentCheckout' => $data,
                ];         
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Fail',
                ];   
            }     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getChildCheckout(Request $request){
        try{
            $data = DB::table('master_check_out')
                ->select('id','desc_check_out')
                ->where('section_id',$request->section_id)
                ->get();
            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'list parent checkin',
                    'childCheckout' => $data,
                ];      
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
                ];    
            }     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getDataSignAndApprover(Request $request){
        try{
            $data = DB::table('general_info_form')
                ->join('users', 'general_info_form.approver_id_checkout', '=', 'users.id')
                ->select('users.name as approver_checkout_name')
                ->where('general_info_form.id', $request->id_form)
                ->first();

            $data2 = DB::table('signs')
            ->select('*')
            ->where('signs.id_form', $request->id_form)
            ->get();

            
            if($data2){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'listActivity',
                    'approverName' => $data,
                    'signData' => $data2
                ];       
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
            ];     
            } 
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getParentAnswerCheckin(Request $request){
        try{
            $data = DB::table('answer_check_in')
                ->join('master_check_in', 'answer_check_in.master_check_in_id', '=', 'master_check_in.id')
                ->join('check_in_section', 'master_check_in.section_id', '=', 'check_in_section.id')
                ->select('check_in_section.*')
                ->where('answer_check_in.general_info_id', $request->id_form)
                ->distinct()
                ->get();

            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'listParent',
                    'parentAnswerList' => $data,
                ];       
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
            ];     
            } 
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getChildAnswerCheckin(Request $request){
        try{
            $data = DB::table('answer_check_in')
                ->join('master_check_in', 'answer_check_in.master_check_in_id', '=', 'master_check_in.id')
                ->select('master_check_in.id as question_id','master_check_in.desc_check_in as question','answer_check_in.id as answer_id','answer_check_in.approve_check_in'
                ,'answer_check_in.decline_check_in','answer_check_in.na_check_in','answer_check_in.annotation_check_in')
                ->where('answer_check_in.general_info_id', $request->id_form)
                ->where('master_check_in.section_id', $request->section_id)
                ->get();

            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'listChild',
                    'childAnswerList' => $data,
                ];       
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
            ];     
            } 
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getParentAnswerCheckout(Request $request){
        try{
            $data = DB::table('answer_check_out')
                ->join('master_check_out', 'answer_check_out.master_check_out_id', '=', 'master_check_out.id')
                ->join('check_out_section', 'master_check_out.section_id', '=', 'check_out_section.id')
                ->select('check_out_section.*')
                ->where('answer_check_out.general_info_id', $request->id_form)
                ->distinct()
                ->get();

            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'listParent',
                    'parentAnswerCheckoutList' => $data,
                ];       
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
            ];     
            } 
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getChildAnswerCheckout(Request $request){
        try{
            $data = DB::table('answer_check_out')
                ->join('master_check_out', 'answer_check_out.master_check_out_id', '=', 'master_check_out.id')
                ->select('master_check_out.id as question_id','master_check_out.desc_check_out as question','answer_check_out.id as answer_id','answer_check_out.approve_check_out'
                ,'answer_check_out.decline_check_out','answer_check_out.na_check_out','answer_check_out.annotation_check_out')
                ->where('answer_check_out.general_info_id', $request->id_form)
                ->where('master_check_out.section_id', $request->section_id)
                ->get();

            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'listChild',
                    'childAnswerCheckoutList' => $data,
                ];       
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
            ];     
            } 
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getEvidences(Request $request){
        try{
            $data=Evidences::where('id_form','=',$request->id_form)->get();
            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'listEvidence',
                    'evidenceList' => $data,
                ];       
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
            ];     
            } 
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getTrucks(){
        try{
            $data = DB::table('trucks')
                ->select('trucks.*')
                ->where('trucks.is_active', '1')
                ->orderBy('trucks.id', 'ASC')
                ->get();

            
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'listTrucks',
                    'trucksList' => $data,
                ];       
            }else{
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Fail',
            ];     
            } 
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Fail',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function deleteActivity(Request $request){
        try{
            $data=GeneralInfoForm::where('id','=',$request->id_form)->first();
            $data->isDeleted = '1';
            $data->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateForm(Request $request){
        try{
            $data=GeneralInfoForm::where('id','=',$request->id_form)->first();        

            if($request->vehicle_number != ""){
                $data->vehicle_number = $request->vehicle_number;
            }
            if($request->tank_number != ""){
                $data->tank_number = $request->tank_number;
            }
            if($request->DO_number != ""){
                $data->DO_number = $request->DO_number;
            }
            if($request->product_name != ""){
                $data->product_name = $request->product_name;
            }
            if($request->vehicle_certificate_expiry_date != ""){
                $data->vehicle_certificate_expiry_date = $request->vehicle_certificate_expiry_date;
            }
            if($request->kir_expiry_date != ""){
                $data->kir_expiry_date = $request->kir_expiry_date;
            }
            if($request->vehicle_type != ""){
                $data->vehicle_type = $request->vehicle_type;
            }
            if($request->carrier_name != ""){
                $data->carrier_name = $request->carrier_name;
            }
            if($request->transporter_type != ""){
                $data->transporter_type = $request->transporter_type;
            }
            if($request->annotation != ""){
                $data->annotation = $request->annotation;
            }
            if($request->destination != ""){
                $data->destination = $request->destination;
            }
            $data->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateAnswerCheckin(Request $request){
        try{
            $data=AnswerCheckins::where('id','=',$request->id_answer)->first();
            if($request->approve_check_in != "3"){
                $data->approve_check_in = $request->approve_check_in;
            }
            if($request->decline_check_in != "3"){
                $data->decline_check_in = $request->decline_check_in;
            }
            if($request->na_check_in != "3"){
                $data->na_check_in = $request->na_check_in;
            }
            if($request->annotation_check_in != ""){
                $data->annotation_check_in = $request->annotation_check_in;
            }else if($request->annotation_check_in == ""){
                $data->annotation_check_in = "-";
            }                        
            $data->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function updateAnswerCheckout(Request $request){
        try{
            $data=AnswerCheckouts::where('id','=',$request->id_answer)->first();
            if($request->approve_check_out != "3"){
                $data->approve_check_out = $request->approve_check_out;
            }
            if($request->decline_check_out != "3"){
                $data->decline_check_out = $request->decline_check_out;
            }
            if($request->na_check_out != "3"){
                $data->na_check_out = $request->na_check_out;
            }        
            if($request->annotation_check_in != ""){
                $data->annotation_check_in = $request->annotation_check_in;
            }else if($request->annotation_check_in == ""){
                $data->annotation_check_in = "-";
            }    
            $data->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
}