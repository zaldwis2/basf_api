<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// $router->post('/register', 'ExampleController@register');

// Auth Controller
$router->post('/register', 'AuthController@register');
$router->get('/login', 'AuthController@login2');
$router->get('/getDataUser', 'AuthController@getDataUser');
$router->put('/updateDataUser', 'AuthController@updateDataUser');
$router->get('/getActivity', 'AuthController@getActivity');
$router->get('/getCheckinCount', 'AuthController@getCheckinCount');
$router->get('/getParentCheckin', 'AuthController@getParentCheckin');
$router->get('/getChildCheckin', 'AuthController@getChildCheckin');
$router->get('/getTransporterType', 'AuthController@getTransporterType');
$router->get('/getDriver', 'AuthController@getDriver');
$router->post('/saveForm', 'AuthController@saveForm');
$router->post('/saveAnswer', 'AuthController@saveAnswer');
$router->put('/addNCM', 'AuthController@addNCM');
$router->post('/saveAnswerCheckout', 'AuthController@saveAnswerCheckout');
$router->post('/saveSignCheckout', 'AuthController@saveSignCheckout');
$router->get('/getParentCheckout', 'AuthController@getParentCheckout');
$router->get('/getChildCheckout', 'AuthController@getChildCheckout');
$router->get('/getDataSignAndApprover', 'AuthController@getDataSignAndApprover');
$router->get('/getParentAnswerCheckin', 'AuthController@getParentAnswerCheckin');
$router->get('/getChildAnswerCheckin', 'AuthController@getChildAnswerCheckin');
$router->get('/getParentAnswerCheckout', 'AuthController@getParentAnswerCheckout');
$router->get('/getChildAnswerCheckout', 'AuthController@getChildAnswerCheckout');
$router->get('/getEvidences', 'AuthController@getEvidences');
$router->get('/getTrucks', 'AuthController@getTrucks');
$router->put('/deleteActivity', 'AuthController@deleteActivity');
$router->put('/updateForm', 'AuthController@updateForm');
$router->put('/updateAnswerCheckin', 'AuthController@updateAnswerCheckin');
$router->put('/updateAnswerCheckout', 'AuthController@updateAnswerCheckout');
